#!/usr/bin/python3

import pymongo
import time
from tqdm import tqdm
from domain.ibp import IBP

mongo_client = pymongo.MongoClient(host=['localhost:27017', 'localhost:27018', 'localhost:27019'],
                                   username="admin",
                                   password="admin123",
                                   replicaset='rs0')

use_db = mongo_client.get_database("gateway_db")  # gateway_db is the db name
# use_db = mongo_client["gateway_db"]
# use_db = mongo_client.gateway_db
#  The three lines above are the same.

use_collection = use_db.get_collection("ibp")  # ibp is the collection name like table name
# use_collection = use_db["ibp"]
# use_collection = use_db.ibp
#  The three lines above are the same.

''' 
filter by gateway_mac_address.
'''
startTime = time.time()
all_ibp_dics = use_collection.find(filter={"gateway_mac_address": "00-0a-31-26-7d"})
all_ibp_objects = [IBP.decode_mongo_data(doc) for doc in all_ibp_dics]
for ibp in tqdm(all_ibp_objects):
    print(ibp.__dict__)


'''
filter by measurement_datetime and sort the result.
'''
# all_ibp_dics = use_collection.find(filter={"measurement_datetime": {"$lt": (time.time()-10)*1000}}).sort("measurement_datetime")
# less then

all_ibp_dics = use_collection.find(filter={"measurement_datetime": {"$gt": (time.time()-300)*1000}}).sort("measurement_datetime")
# greater then
all_ibp_objects = [IBP.decode_mongo_data(doc) for doc in all_ibp_dics]
for ibp in tqdm(all_ibp_objects):
    print(ibp.__dict__)

print("Total cost time: " + str(time.time() - startTime))
